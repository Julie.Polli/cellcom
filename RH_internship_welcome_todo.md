auteur: Julien.Jespersen@unige.ch

date: 2019-10-09

Sécurité ⛑
============================================

### Feu


### Urgences 
- sorties de secours
- alarme
- point de rassemblement
- accidents

Horaire ⏱
============================================
- horaire de travail
- pauses
- heures complémentaires / supplémentaires
- vacances / congés


Vie ensemble 😇
============================================
### open space
- dire boujour au début de la journée
- aurevoir à la fin de la journée
- présentation autres collaborateurs

### places partagées
- rangement
- souris / devices : OFF


### cuisine frigo
- frigo
- vaiselle
- café


Communications 📝
============================================

### par oral
- limiter au possible
- les paroles s'envolent

### par écrit
- les écrits restent
- ne pas raconter sa vie
- aller à l'essentiel
- protocol habituel

- login ordinateurs
- login website, admins
- login boîtes e-mail
- login réseaux sociaux

Poste de travail
============================================
- clé
- carte d'accès
- imprimante
- disques réseau


### Matériel vidéo
- rangement
- allumé / étein
- piles
- câbles

### Softwares video / graphic
- quels logiciels
- quels formats de fichiers
- environnment de travail
- préférences par défault

### Fichiers
- "Our naming convention" 😠
- serveurs, disques, sauvegardes
- dossiers

### Formations
- inscription formations complémentaires
- auto-formations
- partage connaissances
- conférences / cours publics
