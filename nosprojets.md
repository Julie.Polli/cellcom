# Tâches Julien

## RH
- DIFE encadrement stagiaire
- DIFE recrutement stagiaire
- DIFE encadrement graphiste
- DIFE encadrement community manager
- DIFE organisation/modération séances webmasters

## projets web backend/dev
- AC prototype de API
- AC nouvelle admin "GEA"
- AC prototype de catalogue
- AC update et correctifs du scanner ccu
- AC génération planches de billets ccu
- AC génération envellopes ccu
- ADM creation, rédaction, envoi newsletter

## projets web frontend
- ADM rappatriement quelques pages
- DIFE adaptation des modules C5
- AC automatisation pages billets offerts
- SPO prototype homepage
- OVE layout 2016
- PSS layout 2016
- AC update/correction newsletter

## projets graphique
- AC ccu revue
- AC ccu picto
- SEA relecture / correction "Guide pédago." en anglais
- SEA création "Guide création nouveau programmes"
- 